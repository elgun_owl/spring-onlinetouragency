package com.example.tour.model;



import org.springframework.stereotype.Service;


import javax.persistence.*;

@Entity
@Service
public class Tour {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    public int id;
    public String date;
    public String name;
    public String description;
    public String begin_date;
    public String end_date;
    public int price;
    public String status;


    public Tour(){}

    public Tour(String name,String date, String description, String begin_date, String end_date, int price, String status) {
        this.name = name;
        this.date = date;
        this.description = description;
        this.begin_date = begin_date;
        this.end_date = end_date;
        this.price = price;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
