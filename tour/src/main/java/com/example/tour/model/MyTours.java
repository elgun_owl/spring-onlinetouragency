package com.example.tour.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class MyTours {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
    public String date;
    public String name;
    public String begin_date;
    public String end_date;
    public String price;
    public String status;
    public String priceStatus = "Odenilmeyib";



    public MyTours(String date, String name, String begin_date, String end_date, String price, String status) {
        this.date = date;
        this.name = name;
        this.begin_date = begin_date;
        this.end_date = end_date;
        this.price = price;
        this.status = status;
    }
    public  MyTours(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(String priceStatus) {
        this.priceStatus = priceStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}


