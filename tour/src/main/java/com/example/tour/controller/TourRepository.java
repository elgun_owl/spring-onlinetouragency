package com.example.tour.controller;

import com.example.tour.model.MyTours;
import com.example.tour.model.Tour;
import com.example.tour.repo.MyTourRepository;
import com.example.tour.repo.ToursRepository;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class TourRepository {


    @Autowired
    private ToursRepository toursRepository;
    @Autowired
    private MyTourRepository myTourRepository;


    @GetMapping("/")
    public String greeting(Model model) {
        return "home";
    }
    @GetMapping("/home")
    public String home(Model model) {
        return "home";
    }

    @GetMapping("/basket")
    public String bucket(Model model) {
        Iterable<MyTours> myTours = myTourRepository.findAll();
        model.addAttribute("myTours", myTours);
        return "basket";
    }

    @GetMapping("/tour")
    public String tour(Model model) {
        Iterable<Tour> tours = toursRepository.findAll();
        model.addAttribute("tours",tours);
        return "tour";
    }

    @GetMapping("/tour/add")
    public String tourAdd(Model model) {
        return "tourAdd";
    }

    @GetMapping(value = "/tour/{id}/edit")
    public String selectFindById(@PathVariable(value = "id") int id, Model model) {
        Optional<Tour> tour =  toursRepository.findById(id);
        ArrayList<Tour> res = new ArrayList<>();
        tour.ifPresent(res::add);
        model.addAttribute("tour",res);
        return "tourEdit";
    }


    @PostMapping("/tour/add")
    public String tourPostAdd(@RequestParam String name, @RequestParam String add_time, @RequestParam String start_time,
                              @RequestParam String end_time, @RequestParam int price,
                              @RequestParam String description, @RequestParam String status, Model model){
        Tour tour = new Tour(name,add_time,description,start_time,end_time,price,status);
        toursRepository.save(tour);
        return "redirect:/tour";
    }
    @PostMapping("/tour/addBucket")
    public String tourPostAddBucket(@RequestParam String name, @RequestParam String add_time, @RequestParam String start_time,
                              @RequestParam String end_time, @RequestParam String price,
                              @RequestParam String description, @RequestParam String status, Model model){
        MyTours myTours = new MyTours(add_time,name,start_time,end_time,price,status);
        myTourRepository.save(myTours);
        return "redirect:/basket";
    }

    @PostMapping("/tour/{id}/edit")
    public String tourPostUpdate(@PathVariable (value =  "id") int id,@RequestParam String name,@RequestParam String price,
                                 @RequestParam String description, @RequestParam String status){
        Tour tour = toursRepository.findById(id).orElseThrow();
        tour.setName(name);
        tour.setDescription(description);
        tour.setStatus(status);
        int priceInt = Integer.valueOf(price);
        tour.setPrice(priceInt);
        toursRepository.save(tour);
        return "redirect:/tour";
    }

    @PostMapping("/tour/{id}/pay")
    public String tourPriceUpdate(@PathVariable (value =  "id") int id)
    {
        MyTours myTours = myTourRepository.findById(id).orElseThrow();
        myTours.setPriceStatus("Odenildi");
        myTourRepository.save(myTours);
        return "redirect:/basket";
    }

    @PostMapping("/tour/{id}/remove")
    public String tourPostDelete(@PathVariable (value =  "id") int id, Model model){
        Tour tour = toursRepository.findById(id).orElseThrow();
        toursRepository.delete(tour);
        return "redirect:/tour";
    }

}

