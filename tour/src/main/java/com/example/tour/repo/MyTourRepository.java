package com.example.tour.repo;

import com.example.tour.model.MyTours;
import com.example.tour.model.Tour;
import org.springframework.data.repository.CrudRepository;

public interface MyTourRepository extends CrudRepository<MyTours,Integer> {
}
