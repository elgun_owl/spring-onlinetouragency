package com.example.tour.repo;

import com.example.tour.model.Tour;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Tour,Long> {
}
