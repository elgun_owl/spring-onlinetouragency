package com.example.tour.repo;

import com.example.tour.model.Tour;
import org.springframework.data.repository.CrudRepository;

public interface ToursRepository extends CrudRepository<Tour,Integer> {
}
